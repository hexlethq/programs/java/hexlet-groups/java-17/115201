package Hexlet.java.exercises;

import java.util.Scanner;

public class Battleship {
    static final int FILED_LENGTH = 10;
    public static void main(String[] args){
        System.out.println("Player 1, please, input your name");

        Scanner scanner = new Scanner(System.in);
        String player1Name = scanner.nextLine();
        System.out.println("Hello," + player1Name + "!");

        System.out.println("Player 2, please, input your name");
        String player2Name = scanner.nextLine();
        System.out.println("Hello," + player2Name + "!");

        drawBattleField();
    }

    static void drawBattleField() {
        String horizon = "  ";
        for(int i = 0; i < FILED_LENGTH; i++){
            horizon += " " + i;
        }
        System.out.println(horizon);

        for(int i = 0; i < FILED_LENGTH; i++){
            System.out.println(i);
        }


    }

}
